const express = require("express");
let router = express.Router();

router.get("/:path", (req, res) => {
  res.status(200).json({
    stage: process.env.API_STAGE,
    msg: "hello world",
    path: req.params.path,
    version: "1.0.0",
  });
});

module.exports = router;
